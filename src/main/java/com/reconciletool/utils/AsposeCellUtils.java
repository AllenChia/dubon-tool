package com.reconciletool.utils;

import com.aspose.cells.*;
import org.apache.commons.beanutils.BeanUtils;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class AsposeCellUtils {

    public static <T> List<T> read(InputStream inputStream, String[] columns, Class<T> format)
            throws Exception {
        // 最前面的進入點，記得要用 try-with-resource 關 inputstream
        try {
            final Workbook wb = new Workbook(inputStream);
            final Worksheet sheet = wb.getWorksheets().get(0);
            final Cells cells = sheet.getCells();
            cells.deleteRows(0, 1, true); // 刪除第一行
            cells.deleteBlankRows(); // 刪除空行
            final RowCollection rows = cells.getRows();
            final List<T> results = new ArrayList();
            for (Object o : rows) {
                final Row row = (Row) o;
                final T obj = format.newInstance();
                final Map<String, Object> map = new HashMap<>();
                for (int i = 0, columnLength = columns.length; i < columnLength; i++) {
                    final Cell cell = row.get(i);
//          final Object value = customizeValue(cell, format);
                    final Object value = cell.getValue();
                    map.put(columns[i], value);
                }
                BeanUtils.populate(obj, map);
                results.add(obj);
            }
            //      //Create an instance of DeleteOptions class
            //      DeleteOptions options = new DeleteOptions();
            //      //Set UpdateReference property to true;
            //      options.setUpdateReference(true);
            //      //Delete all blank rows and columns
            //      cells.deleteBlankColumns(options);
            //      cells.deleteBlankRows(options);
            return results;
        } catch (Exception e) {
            throw new Exception("Parse Excel error.");
        }
    }

    public static <T> Workbook generate(String[] columns, List<T> datas) {
        final Workbook wb = new Workbook();
        final Worksheet sheet = wb.getWorksheets().get(0);
        final Cells cells = sheet.getCells();
        // write column
        cells.importArray(columns, 0, 0, false);
        // write object list
        ImportTableOptions options = new ImportTableOptions();
        cells.importCustomObjects(datas, 1, 0, options);
        return wb;
    }

    public static <T> Workbook generateSorted(
            String[] displayedColumns, String[] columns, List<T> data) throws Exception {
        final Workbook wb = new Workbook();
        final Worksheet sheet = wb.getWorksheets().get(0);
        final Cells cells = sheet.getCells();
        final int length = columns.length;
        // write header column
        cells.importArray(displayedColumns, 0, 0, false);
        // set header style
        setHeaderStyle(wb, length);
        // vertical write object list
        final HashMap<String, List<Object>> map = toVerticalMap(data);
        for (int i = 0; i < length; i++) {
            cells.importArrayList((ArrayList) map.get(columns[i]), 1, i, true);
        }
        return wb;
    }

    public static void addBorder(
            Workbook wb, Integer firstRow, Integer firstColumn, Integer totalRow, Integer totalColumn) {
        final Cells cells = wb.getWorksheets().get(0).getCells();
        final Range range = cells.createRange(firstRow, firstColumn, totalRow, totalColumn);
        range.setOutlineBorders(CellBorderType.THICK, Color.getBlack());
    }

    public static void setHeaderStyle(Workbook wb, Integer columnSize) throws Exception {
        final Cells cells = wb.getWorksheets().get(0).getCells();
        final Range headerRange = cells.createRange(0, 0, 1, columnSize + 1);
        final Style defaultStyle = wb.getDefaultStyle();
        final Font font = defaultStyle.getFont();
        // 粗體
        font.setBold(true);
        // 微軟正黑體
        font.setName("Microsoft JhengHei");
        // 字體大小
        font.setSize(14);
        headerRange.setStyle(defaultStyle);
        // 自動適應列寬，必須先設定字體 (必須先寫入資料，再使用此方法)
        wb.getWorksheets().get(0).autoFitColumns();
        // 自動適應列高，必須先設定字體 (必須先寫入資料，再使用此方法)
        wb.getWorksheets().get(0).autoFitRows();

    }

    public static void setHeaderAutoFit(Workbook wb, Integer columnSize) throws Exception {
//    final Cells cells = wb.getWorksheets().get(0).getCells();
//    for (int i = 0; i < length; i++) {
//      final Cell cell = cells.get(0, i);
//      cells.setColumnWidthPixel(cell.getColumn(), cell.getWidthOfValue());
//    }
        final Style defaultStyle = wb.getDefaultStyle();

        wb.getWorksheets().get(0).autoFitColumns(0, columnSize);
    }

    public static <T> HashMap<String, List<Object>> toVerticalMap(List<T> list) {
        final Field[] declaredFields = list.get(0).getClass().getDeclaredFields();
        final HashMap<String, List<Object>> map = new HashMap<>();
        for (Field f : declaredFields) {
            final String property = f.getName();
            final List<Object> collect =
                    list.stream()
                            .map(it -> InvokeGetterSetterUtil.invokeGetter(it, property))
                            .collect(Collectors.toList());
            map.put(property, collect);
        }
        return map;
    }

}
