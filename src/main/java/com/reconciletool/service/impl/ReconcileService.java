package com.reconciletool.service.impl;

import com.reconciletool.model.ReconcileDTO;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface ReconcileService {

    List<ReconcileDTO> genReconcileList(Path path, String type) throws Exception;

    void export(List<ReconcileDTO> mofList, Map<String, ReconcileDTO> sapMap, String fileName, String[] displayHeader, String[] header,String pathResult) throws IOException;

    void createFolder(Path pathMof,Path pathSap,Path pathResult);

}
