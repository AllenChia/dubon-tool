package com.reconciletool;

import com.reconciletool.model.ReconcileDTO;
import com.reconciletool.service.impl.ReconcileServiceImpl;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;


//@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DubonCompareToolApplicationTests {

    private static Logger logger = LoggerFactory.getLogger(DubonCompareToolApplicationTests.class);
    String path = System.getProperty("user.dir");
    Path pathMof = Paths.get(path + "\\mof");
    Path pathSap = Paths.get(path + "\\sap");
    Path pathResult = Paths.get(path + "\\result");
    static String[] header = new String[]{"invoiceNumber", "salesAmount", "salesAmountDiff", "vatAmount", "vatSalesAmountDiff"};
    static String[] displayHeader = new String[]{"Invoice No. Comparison 1", "Sales Amount Comparison 1", "Sales Amount Variance 1", "VAT Amount Comparison 1", "VAT Amount Variance 1"};
    ReconcileServiceImpl reconcileService = new ReconcileServiceImpl();
    private static StringBuilder output = new StringBuilder("");

    @Test
    @Order(1)
    void createFolderTest() {
        logger.info("============================ step 1.create folder ================================");
        reconcileService.createFolder(pathMof, pathSap, pathResult);
        output.append("a");
    }

    @Test
    @Order(2)
    void loadDataTest() throws Exception {
        logger.info("============================ step 2.load data ================================");
        String currentPath = System.getProperty("user.dir");
        //gen list & map
        List<ReconcileDTO> mofList = reconcileService.genReconcileList(pathMof, "mof");
        List<ReconcileDTO> sapList = reconcileService.genReconcileList(pathSap, "sap");
        Map<String, ReconcileDTO> mofMap = mofList.stream().collect(Collectors.toMap(ReconcileDTO::getInvoiceNumber, Function.identity()));
        Map<String, ReconcileDTO> sapMap = sapList.stream().collect(Collectors.toMap(ReconcileDTO::getInvoiceNumber, Function.identity()));
        logger.info("mof list is exist:{} ", !mofList.isEmpty());
        logger.info("sap list is exist:{} ", !sapList.isEmpty());
        logger.info("mof map is exist:{} ", !mofMap.isEmpty());
        logger.info("sap map is exist:{} ", !sapMap.isEmpty());
        output.append("b");
    }

    @Test
    @Order(3)
    void exportTest() throws Exception {
        logger.info("============================ step 3.export ================================");
        //gen list & map
        List<ReconcileDTO> mofList = reconcileService.genReconcileList(pathMof, "mof");
        List<ReconcileDTO> sapList = reconcileService.genReconcileList(pathSap, "sap");
        Map<String, ReconcileDTO> mofMap = mofList.stream().collect(Collectors.toMap(ReconcileDTO::getInvoiceNumber, Function.identity()));
        Map<String, ReconcileDTO> sapMap = sapList.stream().collect(Collectors.toMap(ReconcileDTO::getInvoiceNumber, Function.identity()));
        //================ based on Mof excel ================
        reconcileService.export(mofList, sapMap, "以財政部為基礎，比對SAP數據.xls", displayHeader, header, pathResult.toString());
        //================ based on SAP excel ================
        reconcileService.export(sapList, mofMap, "以SAP為基礎,比對財政部數據.xls", displayHeader, header, pathResult.toString());
        output.append("c");
    }

    @AfterAll
    public static void assertOutput() {
        System.out.println(output.toString());
        assertEquals(output.toString(), "abc");
    }


}
